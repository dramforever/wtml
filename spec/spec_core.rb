require 'wtml'
require 'rspec'

describe 'Wtml engine' do
  before do
    @parser = Wtml::Engine.new
  end
  it 'should return the marco-less text' do
    txt = 'foo bar baz'
    txt.should == @parser.render(txt)
  end

  it "should return the hello marco's text" do
    txt = 'foo bar <#hello:foo#>'
    expect = 'foo bar <h1> Hello to foo </h1>'
    expect.should == @parser.render(txt)
  end

  it 'should accept the add_marco method' do
    @parser.add_marco :foo do |arg|
      "Foobar #{arg}"
    end
    txt = 'Foo <#foo:bar#>'
    expect = 'Foo Foobar bar'
    expect.should == @parser.render(txt)
  end

  it 'should be able to recognize multi-line marcos' do
    txt = "foo bar <#hello:foo\nfoo#>"
    expect = "foo bar <h1> Hello to foo\nfoo </h1>"
    expect.should == @parser.render(txt)
  end

  it 'should contain a working binary' do
    `echo "<#hello:foo#>" | bundle exec wtml`.should == '<h1> Hello to foo </h1>'
  end

  it 'should be able to pass file names as arguments to the binary' do
    system 'echo "<#hello:foo#>">/tmp/wtml_temp'
    `bundle exec wtml /tmp/wtml_temp`.should == '<h1> Hello to foo </h1>'
    File.delete '/tmp/wtml_temp'
  end

end

