require 'wtml/default_marcos'

module Wtml
  class Engine
    def initialize(marcos = {})
      @marcos = ::Wtml::DEFAULT_MARCOS.merge marcos
    end

    def add_marco(cmd,&blk)
      @marcos[cmd] = blk
    end

    def render(text)
      buf = text.dup
      match_data = buf.to_enum(:scan, /<#[a-zA-Z][a-zA-Z0-9_]*:(.|\n)+?#>/ ).map { Regexp.last_match }

      # ruler
      # ---------1---------2
      # 12345678901234567890
      # <#cmd: foobar baz #>

      for md in match_data
        off = md.offset(0)
        txt = md.to_s[2..-3]
        cmd = txt.split(':', 2)[0].to_sym
        arg = txt.split(':', 2)[1]
        buf[ off[0]..off[1] ] = @marcos[cmd].call arg
      end

      buf
    end
  end
end
