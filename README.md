Wtml
======

WaTerMeLon the marco engine  [![Code Climate](https://codeclimate.com/github/dramforever/wtml.png)](https://codeclimate.com/github/dramforever/wtml)

Installation
------------

Currently wtml is not pushed on to rubygems.org nor has a released version yet. Please clone the repo and build/install the gem yourself. 

Usage
-------

### As a library

    require 'wtml'
    parser = Wtml::Engine.new
    text = '<#hello:world#>'
    puts parser.render text

### As a cli application
    $ wtml
    <#hello:cli world#>
    [press ctrl-d to see magic]

Contributing
------------

It's github, ok? At least you should know how to contribute. 
